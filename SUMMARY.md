# Table of contents

* [Matt Seemon - Personal Wiki - Home](README.md)

## Technology

* [Build your own NAS with Ubuntu Server](technology/build-your-own-nas/README.md)
  * [Create Bootable USB](technology/build-your-own-nas/create-bootable-usb.md)
  * [Install Ubuntu Server](technology/build-your-own-nas/install-ubuntu-server.md)
  * [Configure Ubuntu Server](technology/build-your-own-nas/configure-ubuntu-server.md)

***

* [Login](https://app.gitbook.com/@matt-seemon/)
* [Useful Linux Command](useful-linux-command.md)
* [Build your own Personal Secrets Vault](build-your-own-personal-secrets-vault/README.md)
  * [Setup PGP Keys](build-your-own-personal-secrets-vault/setup-pgp-keys.md)
  * [Backup PGP Keys](build-your-own-personal-secrets-vault/backup-pgp-keys.md)
  * [Rotating Keys](build-your-own-personal-secrets-vault/rotating-keys.md)
  * [Tools Used](build-your-own-personal-secrets-vault/tools-used.md)

## Games

* [Jurassic World Evolution 2](games/jurassic-world-evolution-2/README.md)
  * [Dinosaur Guide](games/jurassic-world-evolution-2/dinosaur-guide/README.md)
    * [Acrocanthosaurus](games/jurassic-world-evolution-2/dinosaur-guide/acrocanthosaurus.md)

---
description: Here is a list of all the tools used.
---

# Tools Used

| Tool            | Download Url                                                                                                         | Version |
| --------------- | -------------------------------------------------------------------------------------------------------------------- | ------- |
| GnuPG           | [https://gnupg.org/ftp/gcrypt/binary/](https://gnupg.org/ftp/gcrypt/binary/)                                         | 2.3.1   |
| Git for Window  | [https://github.com/git-for-windows/git/releases/latest](https://github.com/git-for-windows/git/releases/latest)     | 2.32.0  |
| PowerShell Core | [https://github.com/PowerShell/PowerShell/releases/latest](https://github.com/PowerShell/PowerShell/releases/latest) | 7.1.3   |

{% hint style="info" %}
Versions mentioned are the latest at the time of publishing this. There could be a newer version available if you are viewing this at a much later date. Ideally none of the commands should change. In case something does not work, please check the documentation of the tool to confirm if there are any changes for those commands.
{% endhint %}

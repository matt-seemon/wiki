---
description: >-
  This sections details the creation of the PGP keys required to encrypt/decrypt
  the secrets.
---

# Setup PGP Keys

## Setup GnuPG

### Create temporary GPG Home \[Optional]

To start with we create temporary home folder for GnuPG to work with. This is an optional step as you can use the default GnuPG home. You can choose not to do this and continue to use the default GnuPG home directory. I prefer to create a temporary folder for the following reasons.

1. Simplifies the backup of the key pair for easy setup in other machines.
2. Provides an extra layer of isolation for these keys which I can wipe securely once I have completed the setup.

{% tabs %}
{% tab title="Microsoft PowerShell" %}
```bash
md C:\Temp\GnuPGHome
$env:GNUPGHOME = "C:\Temp\GnuPGHome"
```
{% endtab %}

{% tab title="Windows Command Prompt" %}
```bash
md C:\Temp\GnuPGHome
set GNUPGHOME=C:\Temp\GnuPGHome
```
{% endtab %}
{% endtabs %}

{% hint style="info" %}
**Note:** Path can be anything you choose
{% endhint %}

{% hint style="danger" %}
Recommend not to have spaces in the path if you are using Windows Command Prompt. If you do have spaces make sure you set the path without quotes.
{% endhint %}

Verify that temporary home is reflecting correctly in GnuPG by executing this command.

```bash
gpg --version
```

You should see an output like this. Line 8 should point to the newly created temporary home.

```bash
gpg (GnuPG) 2.3.1
libgcrypt 1.9.3
Copyright (C) 2021 g10 Code GmbH
License GNU GPL-3.0-or-later <https://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Home: C:\Temp\GnuPGHome
Supported algorithms:
Pubkey: RSA, ELG, DSA, ECDH, ECDSA, EDDSA
Cipher: IDEA, 3DES, CAST5, BLOWFISH, AES, AES192, AES256, TWOFISH,
        CAMELLIA128, CAMELLIA192, CAMELLIA256
AEAD: EAX, OCB
Hash: SHA1, RIPEMD160, SHA256, SHA384, SHA512, SHA224
Compression: Uncompressed, ZIP, ZLIB, BZIP2
```

### Harden GnuPG Configuration

Download the configuration file listed here and save it to the newly created temporary home folder.. (i.e. C:\Temp\GnuPGHome)

{% file src="../.gitbook/assets/gpg.conf" %}
Configuration File
{% endfile %}

## Creating PGP Keys

### Master Key

First step is to create a Master key to be used exclusively for certifications & issuing sub-keys to be used for encryption, decryption, signing & authentication.

{% hint style="info" %}
**Note:** Master key should be kept offline at all times and only used to revoke or issue new sub-keys.
{% endhint %}

During the creation of the Master key you will be prompted to enter and verify a password that is used to secure the PGP Keys. Make sure you keep it handy as you will be asked for this password multiple times during process.

{% hint style="info" %}
**Note:** Make sure the master key password you use is a strong password.
{% endhint %}

To generate a strong password you can use an online strong password generator like [LastPass](https://www.lastpass.com/features/password-generator) or locally using `gpg` itself.

```bash
gpg --gen-random --armor 0 24
```

You should see an output like this.

```bash
JVccl+naCleNp8Lj7w/YWNTaj7PxUUhU
```

{% hint style="info" %}
**Note:** Make sure the password is securely stored or memorized.
{% endhint %}

Now that you have a strong password it is time to generate the master key. Key things to keep in mind when you generate the key.

* Master key should only have Certify capability.
* Master key should be 4096 bit key size.
* Master key should be set to not expire. [See Note #1](setup-pgp-keys.md#notes).

Execute the following to start the key generation.

```bash
gpg --expert --full-generate-key
```

```bash
gpg (GnuPG) 2.3.1; Copyright (C) 2021 g10 Code GmbH
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Please select what kind of key you want:
   (1) RSA and RSA
   (2) DSA and Elgamal
   (3) DSA (sign only)
   (4) RSA (sign only)
   (7) DSA (set your own capabilities)
   (8) RSA (set your own capabilities)
   (9) ECC (sign and encrypt) *default*
  (10) ECC (sign only)
  (11) ECC (set your own capabilities)
  (13) Existing key
  (14) Existing key from card
Your selection? 8
```

Next enter `8` from the options `(8) RSA (set your own capabilities)`.

```bash
Possible actions for this RSA key: Sign Certify Encrypt Authenticate
Current allowed actions: Sign Certify Encrypt

   (S) Toggle the sign capability
   (E) Toggle the encrypt capability
   (A) Toggle the authenticate capability
   (Q) Finished

Your selection? E
```

Enter `E` to toggle off `encrypt` capability.

```bash
Possible actions for this RSA key: Sign Certify Encrypt Authenticate
Current allowed actions: Sign Certify

   (S) Toggle the sign capability
   (E) Toggle the encrypt capability
   (A) Toggle the authenticate capability
   (Q) Finished

Your selection? S
```

Enter `S` to toggle off `sign` capability.

```bash
Possible actions for this RSA key: Sign Certify Encrypt Authenticate
Current allowed actions: Certify

   (S) Toggle the sign capability
   (E) Toggle the encrypt capability
   (A) Toggle the authenticate capability
   (Q) Finished

Your selection? Q
```

Enter `Q` to confirm your selection.

```bash
RSA keys may be between 1024 and 4096 bits long.
What keysize do you want? (3072) 4096
```

Enter `4096` for keysize.

```bash
Requested keysize is 4096 bits
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0) 0
```

Enter `0` to set key does not expire.

```bash
Key does not expire at all
Is this correct? (y/N) Y
```

Enter `Y` to confirm key settings.

Next GnuPG need the primary users name and email address to construct the key identity.

```bash
Real name: Name Surname
```

Enter the primary key user name.

```bash
Email address: name.surname@domain.com
```

Enter the primary key user email.

```bash
Comment: For Secrets
```

Enter a comment. Optional.

```bash
You selected this USER-ID:
    "Name Surname (For Secrets) <name.surname@domain.com>"

Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? O
```

Enter `O` to confirm the identity details.

```bash
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
gpg: key 0x7EF1EDAD3B00E910 marked as ultimately trusted
gpg: directory 'C:\\Temp\\GnuPGHome\\openpgp-revocs.d' created
gpg: revocation certificate stored as 'C:\\Temp\\GnuPGHome\\openpgp-revocs.d\\272DBADA98A382CCA71CA2D67EF1EDAD3B00E910.rev'
public and secret key created and signed.

pub   rsa4096/0x7EF1EDAD3B00E910 2021-08-05 [C]
      Key fingerprint = 272D BADA 98A3 82CC A71C  A2D6 7EF1 EDAD 3B00 E910
uid                              Name Surname (For Secrets) <name.surname@domain.com>
```

Set the key ID as an environment variable `KEYID` for later use.

{% tabs %}
{% tab title="Microsoft PowerShell" %}
```bash
$env:KEYID = "0x7EF1EDAD3B00E910"
```
{% endtab %}

{% tab title="Windows Command Prompt" %}
```
set KEYID=0x7EF1EDAD3B00E910
```
{% endtab %}
{% endtabs %}

### Sub Keys

We start by editing the Master key to add the sub-keys. Key considerations to remember.

* Sub keys should be 4096 bit size keys.
* Sub keys should have expiry date.

{% hint style="info" %}
* I recommend 1 year as the expiration duration, however you can choose any expiry duration of your choice.&#x20;
* The sub-keys can be renewed using the offline Master key. See [rotating keys](rotating-keys.md).
{% endhint %}

{% tabs %}
{% tab title="Microsoft PowerShell" %}
```bash
gpg --expert --edit-key $env:KEYID
```
{% endtab %}

{% tab title="Windows Command Prompt" %}
```bash
gpg --expert --edit-key %KEYID%
```
{% endtab %}
{% endtabs %}

Enter the above command to get into the gpg key edit mode.

```bash
Secret key is available.

sec  rsa4096/0x7EF1EDAD3B00E910
     created: 2021-08-05  expires: never       usage: C
     trust: ultimate      validity: ultimate
[ultimate] (1). Name Surname (For Secrets) <name.surname@domain.com>     
```

#### Signing Key

We start by creating a key for signing.

```bash
gpg> addkey
```

Enter `addkey` at the `gpg` prompt.

```bash
Please select what kind of key you want:
   (3) DSA (sign only)
   (4) RSA (sign only)
   (5) Elgamal (encrypt only)
   (6) RSA (encrypt only)
   (7) DSA (set your own capabilities)
   (8) RSA (set your own capabilities)
  (10) ECC (sign only)
  (11) ECC (set your own capabilities)
  (12) ECC (encrypt only)
  (13) Existing key
  (14) Existing key from card
Your selection? 4
```

Enter `4` to select `(4) RSA (sign only)`.

```bash
RSA keys may be between 1024 and 4096 bits long.
What keysize do you want? (3072) 4096
```

Enter `4096` for key size.

```bash
Requested keysize is 4096 bits
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0) 1y
```

Enter `1y` for key expiry duration.

```bash
Key expires at 8/6/2022 India Standard Time
Is this correct? (y/N)  y
```

Enter `y` to confirm.

```bash
Really create? (y/N) y
```

Enter `y` again to create the key.

```bash
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.

sec  rsa4096/0x7EF1EDAD3B00E910
     created: 2021-08-05  expires: never       usage: C
     trust: ultimate      validity: ultimate
ssb  rsa4096/0xEF2F23FF89E9A79F
     created: 2021-08-06  expires: 2022-08-06  usage: S
[ultimate] (1). Name Surname (For Secrets) <name.surname@domain.com>
```

#### Encryption Key

Next we create a key for encryption.

```bash
gpg> addkey
```

Enter `addkey` at the `gpg` prompt.

```bash
Please select what kind of key you want:
   (3) DSA (sign only)
   (4) RSA (sign only)
   (5) Elgamal (encrypt only)
   (6) RSA (encrypt only)
   (7) DSA (set your own capabilities)
   (8) RSA (set your own capabilities)
  (10) ECC (sign only)
  (11) ECC (set your own capabilities)
  (12) ECC (encrypt only)
  (13) Existing key
  (14) Existing key from card
Your selection? 6
```

Enter `6` to select `(6) RSA (encrypt only)`.

```bash
RSA keys may be between 1024 and 4096 bits long.
What keysize do you want? (3072) 4096
```

Enter `4096` for key size.

```bash
Requested keysize is 4096 bits
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0) 1y
```

Enter `1y` for key expiry duration.

```bash
Key expires at 8/6/2022 India Standard Time
Is this correct? (y/N)  y
```

Enter `y` to confirm.

```bash
Really create? (y/N) y
```

Enter `y` again to create the key.

```bash
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.

sec  rsa4096/0x7EF1EDAD3B00E910
     created: 2021-08-05  expires: never       usage: C
     trust: ultimate      validity: ultimate
ssb  rsa4096/0xEF2F23FF89E9A79F
     created: 2021-08-06  expires: 2022-08-06  usage: S
ssb  rsa4096/0x3D0780B4465F62A4
     created: 2021-08-06  expires: 2022-08-06  usage: E
[ultimate] (1). Name Surname (For Secrets) <name.surname@domain.com>
```

#### Authentication Key

Finally we create the key for authentication.

```bash
gpg> addkey
```

Enter `addkey` at the `gpg` prompt.

```bash
Please select what kind of key you want:
   (3) DSA (sign only)
   (4) RSA (sign only)
   (5) Elgamal (encrypt only)
   (6) RSA (encrypt only)
   (7) DSA (set your own capabilities)
   (8) RSA (set your own capabilities)
  (10) ECC (sign only)
  (11) ECC (set your own capabilities)
  (12) ECC (encrypt only)
  (13) Existing key
  (14) Existing key from card
Your selection? 6
```

Enter `8` to select `(8) RSA (set your own capabilities)` since GPG does not give you an menu option for authentication only key, and toggle all the required capabilities till on `authenticate` is left as the allowed action.

```bash
Possible actions for this RSA key: Sign Encrypt Authenticate
Current allowed actions: Sign Encrypt

   (S) Toggle the sign capability
   (E) Toggle the encrypt capability
   (A) Toggle the authenticate capability
   (Q) Finished

Your selection? S
```

Enter `S` to disable `sign` capability.

```bash
Possible actions for this RSA key: Sign Encrypt Authenticate
Current allowed actions: Encrypt

   (S) Toggle the sign capability
   (E) Toggle the encrypt capability
   (A) Toggle the authenticate capability
   (Q) Finished

Your selection? E
```

Enter `E` to disable `encrypt` capability.

```bash
Possible actions for this RSA key: Sign Encrypt Authenticate
Current allowed actions:

   (S) Toggle the sign capability
   (E) Toggle the encrypt capability
   (A) Toggle the authenticate capability
   (Q) Finished

Your selection? A
```

Enter `A` to enable `authenticate` capability.

```bash
Possible actions for this RSA key: Sign Encrypt Authenticate
Current allowed actions: Authenticate

   (S) Toggle the sign capability
   (E) Toggle the encrypt capability
   (A) Toggle the authenticate capability
   (Q) Finished

Your selection? Q
```

Enter`Q` to confirm your selection.

```bash
RSA keys may be between 1024 and 4096 bits long.
What keysize do you want? (3072) 4096
```

Enter `4096` for key size.

```bash
Requested keysize is 4096 bits
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0) 1y
```

Enter `1y` for key expiry duration.

```bash
Key expires at 8/7/2022 India Standard Time
Is this correct? (y/N)  y
```

Enter `y` to confirm.

```bash
Really create? (y/N) y
```

Enter `y` again to create the key.

```bash
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.

sec  rsa4096/0x7EF1EDAD3B00E910
     created: 2021-08-05  expires: never       usage: C
     trust: ultimate      validity: ultimate
ssb  rsa4096/0xEF2F23FF89E9A79F
     created: 2021-08-06  expires: 2022-08-06  usage: S
ssb  rsa4096/0x3D0780B4465F62A4
     created: 2021-08-06  expires: 2022-08-06  usage: E
ssb  rsa4096/0xA7B663F18B999317
     created: 2021-08-07  expires: 2022-08-07  usage: A
[ultimate] (1). Name Surname (For Secrets) <name.surname@domain.com>
```

```bash
gpg> save
```

Enter `save` to finish saving the keys.

### Configure additional identities \[Optional]

If you have multiple email addresses you would like associated with the keys, you can configure them as additional identities. You can use the `adduid` command to do the same.

{% tabs %}
{% tab title="Microsoft PowerShell" %}
```bash
gpg --expert --edit-key $env:KEYID
```
{% endtab %}

{% tab title="Windows Command Prompt" %}
```
gpg --expert --edit-key %KEYID%
```
{% endtab %}
{% endtabs %}

Enter the above command to get into the gpg key edit mode.

```bash
Secret key is available.

sec  rsa4096/0x7EF1EDAD3B00E910
     created: 2021-08-05  expires: never       usage: C
     trust: ultimate      validity: ultimate
ssb  rsa4096/0xEF2F23FF89E9A79F
     created: 2021-08-06  expires: 2022-08-06  usage: S
ssb  rsa4096/0x3D0780B4465F62A4
     created: 2021-08-06  expires: 2022-08-06  usage: E
ssb  rsa4096/0xA7B663F18B999317
     created: 2021-08-07  expires: 2022-08-07  usage: A
[ultimate] (1). Name Surname (For Secrets) <name.surname@domain.com>
```

```bash
gpg> adduid
```

Enter `adduid` to start adding new identities.

```bash
Real name: Name Surname
```

Enter the primary key user name.

```bash
Email address: name.surname@domain.com
```

Enter the primary key user email.

```bash
Comment: For Secrets
```

Enter a comment. Optional.

```bash
You selected this USER-ID:
    "Name Surname (For Secrets) <name.surname@another.com>"

Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? O
```

Enter `O` to confirm the identity details.

```bash
sec  rsa4096/0x7EF1EDAD3B00E910
     created: 2021-08-05  expires: never       usage: C
     trust: ultimate      validity: ultimate
ssb  rsa4096/0xEF2F23FF89E9A79F
     created: 2021-08-06  expires: 2022-08-06  usage: S
ssb  rsa4096/0x3D0780B4465F62A4
     created: 2021-08-06  expires: 2022-08-06  usage: E
ssb  rsa4096/0xA7B663F18B999317
     created: 2021-08-07  expires: 2022-08-07  usage: A
[ultimate] (1)  Name Surname (For Secrets) <name.surname@domain.com>
[ unknown] (2). Name Surname (For Secrets) <name.surname@another.com>
```

Next we have to set the trust level for the new identity.

```bash
gpg> trust
```

Enter the command `trust` to set the trust level

```bash
sec  rsa4096/0x7EF1EDAD3B00E910
     created: 2021-08-05  expires: never       usage: C
     trust: ultimate      validity: ultimate
ssb  rsa4096/0xEF2F23FF89E9A79F
     created: 2021-08-06  expires: 2022-08-06  usage: S
ssb  rsa4096/0x3D0780B4465F62A4
     created: 2021-08-06  expires: 2022-08-06  usage: E
ssb  rsa4096/0xA7B663F18B999317
     created: 2021-08-07  expires: 2022-08-07  usage: A
[ultimate] (1)  Name Surname (For Secrets) <name.surname@domain.com>
[ unknown] (2). Name Surname (For Secrets) <name.surname@another.com>

Please decide how far you trust this user to correctly verify other users' keys
(by looking at passports, checking fingerprints from different sources, etc.)

  1 = I don't know or won't say
  2 = I do NOT trust
  3 = I trust marginally
  4 = I trust fully
  5 = I trust ultimately
  m = back to the main menu

Your decision? 5
```

Enter `5` to set the trust level to `ultimate`.

```bash
Do you really want to set this key to ultimate trust? (y/N) y
```

Enter `y` to confirm selection.

```bash
sec  rsa4096/0x7EF1EDAD3B00E910
     created: 2021-08-05  expires: never       usage: C
     trust: ultimate      validity: ultimate
ssb  rsa4096/0xEF2F23FF89E9A79F
     created: 2021-08-06  expires: 2022-08-06  usage: S
ssb  rsa4096/0x3D0780B4465F62A4
     created: 2021-08-06  expires: 2022-08-06  usage: E
ssb  rsa4096/0xA7B663F18B999317
     created: 2021-08-07  expires: 2022-08-07  usage: A
[ultimate] (1)  Name Surname (For Secrets) <name.surname@domain.com>
[ unknown] (2). Name Surname (For Secrets) <name.surname@another.com>
```

{% hint style="info" %}
**Note:** The changes to trust level will not reflect until you save.
{% endhint %}

By default the last identity that you added will be marked as the primary identity. You can use the `primary` command to change that.

To set an identity as primary, you will need to select the identity first.

```bash
gpg> uid 1
```

Enter `uid` followed by the identity number to select the identity.

```bash
sec  rsa4096/0x7EF1EDAD3B00E910
     created: 2021-08-05  expires: never       usage: C
     trust: ultimate      validity: ultimate
ssb  rsa4096/0xEF2F23FF89E9A79F
     created: 2021-08-06  expires: 2022-08-06  usage: S
ssb  rsa4096/0x3D0780B4465F62A4
     created: 2021-08-06  expires: 2022-08-06  usage: E
ssb  rsa4096/0xA7B663F18B999317
     created: 2021-08-07  expires: 2022-08-07  usage: A
[ultimate] (1)* Name Surname (For Secrets) <name.surname@domain.com>
[ unknown] (2). Name Surname (For Secrets) <name.surname@another.com>
```

```bash
gpg> primary
```

Enter `primary` to set the selected identity as primary.

```bash
gpg> save
```

Enter `save` to finish saving the changes.

## Verify PGP keys

You should now verify the keys you created.

```bash
gpg -K
```

Enter the above command to list the keys for verification.

```bash
C:\Temp\GnuPGHome\pubring.kbx
----------------------------------

pub   rsa4096/0x7EF1EDAD3B00E910 2021-08-05 [C]
      Key fingerprint = 272D BADA 98A3 82CC A71C  A2D6 7EF1 EDAD 3B00 E910
uid                   [ultimate] Name Surname (For Secrets) <name.surname@domain.com>
uid                   [ultimate] Name Surname (For Secrets) <name.surname@another.com>
sub   rsa4096/0xEF2F23FF89E9A79F 2021-08-06 [S] [expires: 2022-08-06]
sub   rsa4096/0x3D0780B4465F62A4 2021-08-06 [E] [expires: 2022-08-06]
sub   rsa4096/0xA7B663F18B999317 2021-08-07 [A] [expires: 2022-08-07]
```

## Notes

> 1. Setting an expiry essentially forces you to manage your subkeys and announces to the rest of the world that you are doing so. Setting an expiry on a primary key is ineffective for protecting the key from loss - whoever has the primary key can simply extend its expiry period. Revocation certificates are [better suited](https://security.stackexchange.com/questions/14718/does-openpgp-key-expiration-add-to-security/79386#79386) for this purpose. It may be appropriate for your use case to set expiry dates on subkeys.

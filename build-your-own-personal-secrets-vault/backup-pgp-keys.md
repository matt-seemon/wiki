# Backup PGP Keys

At this point we are going to create backups of your PGP keys. Once you move the keys to the Yubikey they cannot be moved again.

We are going to follow two strategies for the same.

1. Create and encrypted backup of your GPG keyring in its entirety.
2. Export the secrets keys to keep a paper copy of the same.

## Prerequisites for backing up GPG keyring

To begin with, to back your keyring you will need an USB drive to store the backup. Since the keyring will be under a few MB's in size at the most, you don't need an USB drive with large capacity. The smallest capacity would suffice. Make sure you pick one from a trusted brand to ensure it will last long.

At this point you have 3 approaches you can take. The choice is entirely up to you.

1. Encrypt the whole drive, backup the keyring.
2. Created an encrypted partition in the drive to backup the key ring, incase you want to use the drive for other purposes.
3. Create an encrypted virtual hard disk to store the backup and copy the virtual drive to the USB key.

## Prepare drive for encrypted storage

Based on your how you want to maintain your backup, I have listed the steps for each of the three options.

### Create a new virtual hard disk&#x20;

First you will need to start Microsoft PowerShell or Windows Command Prompt in Administrative Mode. This is required as the commands you are going to execute needs elevated privileges and will not work in the normal mode.

{% hint style="info" %}
Microsoft PowerShell provides cmdlets for managing virtual drives. In Windows Command Prompt you will need to enter the disk partition utility `diskpart` first.
{% endhint %}

#### Start Disk Partition Utility \[Windows Command Prompt Only]

{% tabs %}
{% tab title="Windows Command Prompt" %}
```
diskpart
```
{% endtab %}
{% endtabs %}

Type the `diskpart` command and hit enter.

{% tabs %}
{% tab title="Windows Command Prompt" %}
```
Microsoft DiskPart version 10.0.18362.1533

Copyright (C) Microsoft Corporation.
On computer: <COMPUTER-NAME>

DISKPART>
```
{% endtab %}
{% endtabs %}

Before you begin, you need to determine a few things.

1. Location of Virtual Drive
2. Total Size of Virtual Drive
3. Fixed Size or Dynamically expanding (Fixed size would mean the virtual drive file will occupy the same about of space on your computer as that its total size defined. Dynamically expanding would mean that the amount of space occupied by the virtual drive file will be the minimum it needs to store its contents, and will expand up to the final size.)

Since we are only going to store the keyring in the virtual drive, I recommend a dynamically expanding virtual drive of 1 GB Total size.

{% hint style="info" %}
Note: The PowerShell commands to create and manage virtual disks is only available once you install the Hyper-V Management Tools or Hyper-V features on Windows.
{% endhint %}

{% tabs %}
{% tab title="Microsoft PowerShell" %}
```
New-VHD -Path C:\Temp\Vault.vhdx -Dynamic -SizeBytes 1GB
```
{% endtab %}

{% tab title="Windows Command Prompt" %}
```
DISKPART> create vdisk file="C:\Temp\Vault.vhdx" maximum=1000 type=expandable
```
{% endtab %}
{% endtabs %}

Enter the above command to create the virtual hard disk.

{% tabs %}
{% tab title="Microsoft PowerShell" %}
```
ComputerName            : <COMPUTER-NAME>
Path                    : C:\Temp\Vault.vhdx
VhdFormat               : VHDX
VhdType                 : Dynamic
FileSize                : 4194304
Size                    : 1073741824
MinimumSize             :
LogicalSectorSize       : 512
PhysicalSectorSize      : 4096
BlockSize               : 33554432
ParentPath              :
DiskIdentifier          : 6D03371E-F8EE-4821-B07D-C090960C3C20
FragmentationPercentage : 0
Alignment               : 1
Attached                : False
DiskNumber              :
IsPMEMCompatible        : False
AddressAbstractionType  : None
Number                  :
```
{% endtab %}

{% tab title="Windows Command Prompt" %}
```
  100 percent completed

DiskPart successfully created the virtual disk file.
```
{% endtab %}
{% endtabs %}

Before you can use the virtual hard disk, you will need to initialize it and mount it for use.

{% tabs %}
{% tab title="Microsoft PowerShell" %}
```
Mount-VHD -Path C:\Temp\Vault.vhdx
$d = Get-VHD -Path C:\Temp\Vault.vhdx
Initialize-Disk $d.DiskNumber
$p = New-Partition -DriveLetter V -UseMaximumSize -DiskNumber $d.DiskNumber
Format-Volume -FileSystem NTFS -Partition $p -FileSystemLabel "Vault"
```
{% endtab %}

{% tab title="Windows Command Prompt" %}
```
DISKPART> select vdisk file="C:\Temp\Vault.vhdx"
DISKPART> attach vdisk
DISKPART> convert mbr
DISKPART> create partition primary
DISKPART> format fs=ntfs label="Vault" quick
DISKPART> assign letter=v
```
{% endtab %}
{% endtabs %}

Enter the above commands to initialize and mount the virtual hard disk for use.&#x20;

### Create a partition on an existing USB drive

First you will need to start Microsoft PowerShell or Windows Command Prompt in Administrative Mode. This is required as the commands you are going to execute needs elevated privileges and will not work in the normal mode.

{% hint style="info" %}
Microsoft PowerShell provides cmdlets for managing virtual drives. In Windows Command Prompt you will need to enter the disk partition utility `diskpart` first.
{% endhint %}

#### Start Disk Partition Utility \[Windows Command Prompt Only]

{% tabs %}
{% tab title="Windows Command Prompt" %}
```
diskpart
```
{% endtab %}
{% endtabs %}

Type the `diskpart` command and hit enter.

{% tabs %}
{% tab title="Windows Command Prompt" %}
```
Microsoft DiskPart version 10.0.18362.1533

Copyright (C) Microsoft Corporation.
On computer: <COMPUTER-NAME>

DISKPART>
```
{% endtab %}
{% endtabs %}

To begin, you will need to identify the disk number.

{% tabs %}
{% tab title="Microsoft PowerShell" %}
```
Get-Disk
```
{% endtab %}

{% tab title="Windows Command Prompt" %}
```
DISKPART> list disk
```
{% endtab %}
{% endtabs %}

Enter the above command to list all attach disks to your computer. Make a note of the disk number. In this case, the USB drive is disk number **2**.

{% tabs %}
{% tab title="Microsoft PowerShell" %}
```
Number Friendly Name Serial Number                    HealthStatus         OperationalStatus      Total Size Partition
                                                                                                             Style
------ ------------- -------------                    ------------         -----------------      ---------- ----------
1      Msft Virtual…                                  Healthy              Online                       8 GB MBR
0      PM981a NVMe … 3447_5420_4AB7_1189_0025_3846_0… Healthy              Online                  476.94 GB GPT
2      USB SanDisk … 0401d8546d646a93d7d5             Healthy              Online                   28.65 GB MBR
```
{% endtab %}

{% tab title="Windows Command Prompt" %}
```
  Disk ###  Status         Size     Free     Dyn  Gpt
  --------  -------------  -------  -------  ---  ---
  Disk 0    Online          476 GB      0 B        *
  Disk 1    Online            8 GB  1024 KB
  Disk 2    Online           28 GB      0 B
```
{% endtab %}
{% endtabs %}

To create a partition, you will need to clear the drive first.&#x20;

{% hint style="info" %}
Note: Any data stored on the drive will get deleted when you clean the drive. So make sure you backup any important data.
{% endhint %}

{% tabs %}
{% tab title="Microsoft PowerShell" %}
```
Clear-Disk -Number 2 -RemoveData
```
{% endtab %}

{% tab title="Windows Command Prompt" %}
```
DISKPART> select disk 2
DISKPART> clean
```
{% endtab %}
{% endtabs %}

Enter the above command to clear disk and remove any partitions.

{% tabs %}
{% tab title="Microsoft PowerShell" %}
```
Confirm
Are you sure you want to perform this action?
This will erase all data on disk 2 "USB SanDisk 3.2Gen1".
[Y] Yes  [A] Yes to All  [N] No  [L] No to All  [S] Suspend  [?] Help (default is "Y"): Y
```
{% endtab %}
{% endtabs %}

Enter `Y` to confirm. _\[Microsoft PowerShell Only. `diskpart` does not prompt for confirmation]_

{% tabs %}
{% tab title="Microsoft PowerShell" %}
```
$p = New-Partition -DiskNumber 2 -Size 1GB -DriveLetter V
Format-Volume -FileSystem NTFS -Partition $p
```
{% endtab %}

{% tab title="Windows Command Prompt" %}
```
DISKPART> create partition primary size=1024
DISKPART> format fs=ntfs quick
DISKPART> assign letter=Y
```
{% endtab %}
{% endtabs %}

Enter the following commands to create a 1GB partition and format it.

{% hint style="info" %}
If you want to, you can create additional partitions using the above commands. Just make sure you change the `DriveLetter` and `Size` parameters to suit your needs.
{% endhint %}

Once you have all the required partitions created, you can use the below command to create a final partition to use up any free space left.

{% tabs %}
{% tab title="Microsoft PowerShell" %}
```
$p = New-Partition -DiskNumber 2 -UseMaximumSize -DriveLetter X
Format-Volume -FileSystem NTFS -Partition $p
```
{% endtab %}

{% tab title="Windows Command Prompt" %}
```
DISKPART> create partition primary
DISKPART> format fs=ntfs quick
DISKPART> assign letter=X
```
{% endtab %}
{% endtabs %}

Enter the following commands to create a second partition for the rest of the space in the USB drive.

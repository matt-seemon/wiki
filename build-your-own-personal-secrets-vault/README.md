# Build your own Personal Secrets Vault

The following guide walk you through a step by step process of setting a personal secrets vault system using the following technologies.

## Core Principles

* All secrets to be maintained in plain text.
* All secrets to be stored must roll up to a single root folder (e.g. `C:\Vault`).
  * Sub folders can be created under the root to organize the data.
* Each secret will stored in individual files
* All secrets will be encrypted and stored using PGP Private Public Key Cryptography.
* All secrets will be version controlled with Git.
* The git repository will hosted on a remote git service like GitHub.
* All access and decryption to be managed using a security key like Yubico's Yubikey.

## Requirements

1. GnuPG - For creating and maintaining PGP Key Pairs and to encrypt and decrypt the secrets.
2. Git - For managing the Git repository.
3. Yubikey - To store the PGP private keys.
4. \[Optional] Microsoft PowerShell - Needed for setup and run encrypt/decrypt commands. You can also use Windows Command Prompt.
5. \[Optional] Visual Studio Code - Editor to edit secret files. You can also use Windows Notepad.
   1. \[Optional] GnuPG-Tool - An extension to encrypt/decrypt files from within Visual Studio Code.

{% hint style="info" %}
This guide assumes you are using Microsoft PowerShell for the setup. Most of the commands here will work in Windows Command Prompt as well as they are not PowerShell specific. For the exceptions, I will provide the Command Prompt commands as well.
{% endhint %}

{% hint style="info" %}
Links to the various tools used are provided [here](tools-used.md).
{% endhint %}

{% hint style="info" %}
Derived from the excellent guide built by Dr. Duh, which can be found [here](https://github.com/drduh/YubiKey-Guide).
{% endhint %}

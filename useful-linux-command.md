# Useful Linux Command

Delete empty directories recursively

```
find . -type d -empty -delete
```

Delete files recursively

```
find . -name "*.bak" -type f -delete
```


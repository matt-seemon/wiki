# Configure Ubuntu Server

There are a things you should considering setting up before configuring your RAID storage.

## Secure the root user

The first thing I always do is secure the `root` user account.&#x20;

{% hint style="info" %}
By default with Ubuntu 20.04 the `root` account is disable so there is no need to do anything here if it is a fresh installation of Ubuntu.
{% endhint %}

Should you choose to enable the `root` account, simply set its password using the below command in the Ubuntu Server terminal window.

```
sudo passwd root
```

To disable the account, execute the below command.

```
sudo passwd -l root
```

## Secure SSH Access

Next we secure how we access the server using SSH by updating the `/etc/ssh/sshd_config` file. Type the below command in the Ubuntu server terminal window.

```
sudo nano /etc/ssh/sshd_config
```

Next we secure the access to the server by disabling `root` or any user with no password from logging in using **SSH**.

{% hint style="info" %}
By default with Ubuntu 20.04, access to the server for `root` account and users with no password set, using **SSH** is disabled so there is no need to do anything else.&#x20;
{% endhint %}

### Disable `root` account login through SSH

Look for the following line in the file

```
PermitRootLogin yes
```

Change the value `yes` to `no`. This disables `root` account to login via **SSH**.

```
PermitRootLogin no
```

### Disable Login for users with blank passwords

Look for the following line

```
PermitEmptyPasswords yes
```

Change the value `yes` to `no`. This disables any user with no password to login via **SSH**.

```
PermitEmptyPasswords no
```

### Enable Public Key Authentication

Generate a new SSH Keypair to use for authentication. Open Windows command prompt (`cmd`) and type the following command.

{% hint style="info" %}
The next two command `ssh-keygen` and `scp` are executed in a Windows command prompt or PowerShell command prompt on the PC you are going to connect from. The rest of the commands on this page are executed in a Ubuntu server terminal window running on your Windows PC or directly on the Ubuntu Server.
{% endhint %}

```
ssh-keygen
```

First you will be prompted to enter file in which to save. You can press the ENTER key here to accept the default file name (`C:\Users\<user-name>/.ssh/id_rsa`) or specify your own path. Once you enter a passphrase and confirm the passphrase, it will generate your public and private keys as seen in the below output. Both files will have the same name with the exception of the file extension. The private key will not have an extension, while the public key will have the extension `.pub`.

```
Generating public/private rsa key pair.
Enter file in which to save the key (C:\Users\user-name/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in C:\Users\user-name/.ssh/id_rsa.
Your public key has been saved in C:\Users\user-name/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:kcGnh9+Aef5jUva3zGk+Vn5Vk9HONXayD2MkC9n8TbI user-name@host-name
The key's randomart image is:
+---[RSA 2048]----+
|       ..  +    .|
|        .o+ + ++=|
|        o* . =.X*|
|        =.+ . Eo=|
|        S= o . +o|
|          o +   +|
|           + . .o|
|          . + ++=|
|           o .+B+|
+----[SHA256]-----+
```

Next we need to copy the public key to the Ubuntu server. You can do this with the following command.&#x20;

```
scp C:\Users\<user-name>\.ssh\id_rsa.pub matt@ubuntu-test:~
```

Once the file is copied to the public key to the Ubuntu server, you exit the Windows command prompt.



Once any or all of the above changes are made you need to check the configuration file for any syntax errors.

```
sudo sshd -t
```

For the changes to take effect you need to restart SSH service. Execute the following command.

```
sudo service sshd restart
```

SSH

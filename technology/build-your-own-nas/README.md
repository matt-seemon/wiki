# Build your own NAS with Ubuntu Server

The below guide documents the steps required to install Ubuntu Server, setup & configure a RAID5 NAS System.

{% content-ref url="create-bootable-usb.md" %}
[create-bootable-usb.md](create-bootable-usb.md)
{% endcontent-ref %}

{% content-ref url="install-ubuntu-server.md" %}
[install-ubuntu-server.md](install-ubuntu-server.md)
{% endcontent-ref %}

{% content-ref url="configure-ubuntu-server.md" %}
[configure-ubuntu-server.md](configure-ubuntu-server.md)
{% endcontent-ref %}


# Install Ubuntu Server

To start the installation of Ubuntu, plug in the bootable USB drive created in the [previous step](create-bootable-usb.md) into an USB port on the PC you want to install the operating system and power on the system.&#x20;

Boot from the USB drive by pressing the PC's boot key (The boot key depends on the motherboard manufacturer, typically it is **F12**).

{% hint style="info" %}
At the time of writing this, the current LTS version of Ubuntu is 20.04.1. The screen shots listed here are for version 20.04 LTS. These may vary in future versions.

This guide assumes that the language of choice is **English**.
{% endhint %}

## Welcome

Once the system is booted up, it will start the installation wizard with the **Welcome** screen, prompting you to select your language. Use the **UP** and **DOWN** arrow keys to change to the language of your choice and press the **ENTER** key to proceed.&#x20;

The rest of the installation wizard will be in the language you choose here.

![](<../../.gitbook/assets/Ubuntu 01.png>)

## Installer update&#x20;

The installation wizard will now check to see if there is an updated version of the installer available and take you to the **Installer update available** screen, where you can choose to either _Update to the new Installer_ or _Continue without updating_ . Use the **UP** and **DOWN** arrow keys to make your choice and press the **ENTER** key to proceed.&#x20;

![](<../../.gitbook/assets/Ubuntu 02.png>)

If you choose to Update, the new version of the installer will be download and the installer will be restarted automatically. Progress of the update will be displayed and an option to _Cancel update_ will be provided. Do nothing to let the update complete or press the **ENTER** key to cancel the update.&#x20;

![](<../../.gitbook/assets/Ubuntu 03.png>)

## Keyboard configuration

Next you need to choose the keyboard configuration. Use the **UP** and **DOWN** arrow keys to highlight the options (_Layout_ and _Variant_) available and press the **ENTER** key to change the option. Highlight _Identify Keyboard_ and press the **ENTER** to detect your keyboard layout automatically. When you have made your choices, highlight _Done_ and press the **ENTER** key to proceed.

![](<../../.gitbook/assets/Ubuntu 04.png>)

## Network Configuration

Next you need to configure the network settings. If your PC is connected to a network, it should automatically receive an IP address from your router.  Use the **UP** and **DOWN** arrow keys to highlight the options available and press the **ENTER** key to change the option. When you have made your choices, highlight _Done_ and press the **ENTER** key to proceed.

![](<../../.gitbook/assets/Ubuntu 05.png>)

## Proxy Configuration

Next you need to configure the proxy settings should your network need it to connect to the internet. Use the **UP** and **DOWN** arrow keys to highlight the _Proxy address_ field to update it. When you are done with the changes, highlight _Done_ and press the **ENTER** key to proceed.

![](<../../.gitbook/assets/Ubuntu 06.png>)

## Ubuntu archive mirror configuration

On the next screen, Configure Ubuntu archive mirror**,** the wizard will automatically select the nearest archive based on your location. Press the **ENTER** key to proceed.

![](<../../.gitbook/assets/Ubuntu 07.png>)

## Storage Configuration

Next you need to configure your storage. You can choose to let the wizard provide a base structure which you can then customize to your needs or choose to go with a complete custom layout. For the purpose of this guide, we will choose the former. Use the **UP** and **DOWN** arrow keys to highlight the options available and press the **ENTER** key to change the option.

1. Ensure the _Use an entire disk_ option is selected.
2. Ensure the right Hard disk is selected, in case you have more than one hard disk connected to the PC.
3. Ensure the _Set up this disk as an LVM group_ is selected.

Press the **ENTER** key to proceed.

{% hint style="info" %}
For the purpose of this guide, I have configured my test machine with a 250 GB disk drive and 16 GB of RAM. The partitioning sizes are dependent on the overall size. You should plan your partitioning based on your available space. I have included a minimum required size for each partition. Any additional space is a bonus.
{% endhint %}

![](<../../.gitbook/assets/Ubuntu 08.png>)

Now you can customize the base configuration defined by the installation wizard to your requirement. By default, the wizard will create 3 physical partitions `bios_grub`, `/boot`,  and a physical partition for the logical volume group `ubuntu-vg`. Initially`ubuntu-vg` contains only the logical volume for root (`/`). We will now customize this to resize the root (`/`) logical volume and add 2 logical volumes (`swap`, `/home`).&#x20;

{% hint style="info" %}
You don't need any additional changes other than the default configuration, however it is recommended you at least have a `/home` and `swap` logical volumes along with the `/` logical volumes.
{% endhint %}

![](<../../.gitbook/assets/Ubuntu 09.png>)

First you resize the the root (`/`) logical volume. Under **USED DEVICES**, navigate to the `/` logical volume as highlighted below and press the **ENTER** key to get the partitioning options available. Then select **Edit** and press the **ENTER** key again to proceed.

![](<../../.gitbook/assets/Ubuntu 10.png>)

Edit the _Size_ as defined below and leave the rest of the options to the default values. Highlight _Save_ option and press the **ENTER** key to proceed.

![](<../../.gitbook/assets/Ubuntu 11.png>)

Next you create the swap logical volume. Under **USED DEVICES**, navigate to the _LVM volume group_ as highlighted below and press the **ENTER** key to get the partitioning options available. Then select **Create Logical Volume** and press the **ENTER** key again to proceed.

![](<../../.gitbook/assets/Ubuntu 12.png>)

Enter the _Name_ and _Size_ as defined below. Set the _Format_ option to _swap_. Highlight _Create_ option and press the **ENTER** key to proceed.

{% hint style="info" %}
Set the swap size to twice the amount of RAM on your system.
{% endhint %}

![](<../../.gitbook/assets/Ubuntu 13.png>)

Next you create the swap logical volume. Under **USED DEVICES**, navigate to the _LVM volume group_ as highlighted below and press the **ENTER** key to get the partitioning options available. Then select **Create Logical Volume** and press the **ENTER** key again.

Enter the _Name_ and _Size_ as defined below and leave the rest of the options to the default values. Highlight _Create_ option and press the **ENTER** key to proceed.

![](<../../.gitbook/assets/Ubuntu 14.png>)

Your current system summary should now have a `/`, `/boot`, `/home`, and `swap` partition/logical volumes as seen below. To finalize the changes to the disk drive, Navigate to _Done_ option, and press the **ENTER** key to proceed.

![](<../../.gitbook/assets/Ubuntu 15.png>)

You will now need to confirm this action. Highlight the _Continue_ option and press the **ENTER** key to proceed.

![](<../../.gitbook/assets/Ubuntu 16.png>)

### Physical Partitions

| Partition  | Type | Mount Point | Size            |
| ---------- | ---- | ----------- | --------------- |
| bios\_grub |      |             | 1 MB            |
| boot       | EXT4 | /boot       | 1 GB            |
| ubuntu-vg  | LVM  |             | Remaining Space |

### Logical Volumes

| Logical Volume | Type | Mount Point | Size            | <p>Minimum </p><p>Recommended Size</p> |
| -------------- | ---- | ----------- | --------------- | -------------------------------------- |
| Root           | EXT4 | /           | 120 GB          | 5-6 GB                                 |
| Swap           | SWAP |             | 32 GB           | Size of RAM                            |
| Home           | EXT4 | /home       | Remaining Space | 100 MB per user.                       |

## Profile Setup

On the next screen, you will need to enter the profile information for your system. Enter _Your name_, _Your server's name_, _Pick a username_, _Choose a password_ and _Confirm your password_ used to login to the system. Once all options are updated, highlight _Done_ and press the **ENTER** key to proceed.

![](<../../.gitbook/assets/Ubuntu 17.png>)

## SSH Configuration

Next you will be prompted to Install Open SSH Server package for secure remote access. Use the **UP** and **DOWN** arrow keys to highlight the options available and press the **ENTER** key to change the option. Once all options are updated, highlight _Done_ and press the **ENTER** key to proceed.

![](<../../.gitbook/assets/Ubuntu 18.png>)

## Additional Server Snaps

If you would like to install additional server snaps, use the **UP** and **DOWN** arrow keys to highlight the options available and press the **SPACE** key to select/deselect the option. Press the ENTER key to get more details and choose a specific version of the component. Once all options are updated, highlight _Done_ and press the **ENTER** key to proceed.

![](<../../.gitbook/assets/Ubuntu 19.png>)

## Installing Ubuntu

At this moment the installation of Ubuntu server will start and the progress will be reported on the screen.

![](<../../.gitbook/assets/Ubuntu 20.png>)

Once the installation is complete, highlight _Reboot_ and press the **ENTER** key to reboot the system.

![](<../../.gitbook/assets/Ubuntu 21.png>)

## Login to Ubuntu Server

Once the server is rebooted, you will be able to login to your new installation. Enter the _Login_ and _Password_ to login.

![](<../../.gitbook/assets/Ubuntu 22.png>)

If username and password is correct you will be logged into the system and will be welcomed with the below messages.

![](<../../.gitbook/assets/Ubuntu 23.png>)

# Create Bootable USB

## Prerequisites&#x20;

* ​[Rufus](https://rufus.ie/), a free and open source USB drive writing tool (Portable Edition preferred)
* Ubuntu Server ISO file. Download links available [here](https://ubuntu.com/download/server)
* 4 GB or larger USB drive

To begin with, plug in the USB Drive you are going to run the Ubuntu Server installer from into a PC.

{% hint style="info" %}
This guide assumes you are using a windows PC to get the bootable USB drive created. You can find plenty of tutorials on the internet for how to do the same on MacOS and Linux.
{% endhint %}

Next double click the downloaded file for Rufus to start the program. If you downloaded the installer, you will need to install the application first.

Once the application window is displayed, click on **SELECT** to choose the ISO file.

![](<../../.gitbook/assets/Rufus 01.png>)

Select the downloaded Ubuntu server ISO file and click **Open**.

![](<../../.gitbook/assets/Rufus 02.png>)

Leave the rest of the options with their default values and click **START** to initiate the write process.

![](<../../.gitbook/assets/Rufus 03.png>)

At this time you may be alerted that Rufus requires additional files to complete writing the ISO. If this dialog box appears, select **Yes** to continue.

![](<../../.gitbook/assets/Rufus 04.png>)

Next Rufus will alert that the Ubuntu ISO is an _ISOHybrid image_. Keep _Write in ISO Image mode_ selected and click on **OK** to continue.

![](<../../.gitbook/assets/Rufus 05.png>)

Next Rufus will warn you that all data on your selected USB device is about to be destroyed. This is a good moment to double check you’ve selected the correct device before clicking **OK**.

![](<../../.gitbook/assets/Rufus 06.png>)

Now, Rufus will start writing the ISO file to the UBS drive. The progress bar will tell you how much more of the write operation is pending to complete.

![](<../../.gitbook/assets/Rufus 07.png>)

When the writing is complete the progress bar with have the text **READY** displayed in the center. Click on **CLOSE** to close the utility

![](<../../.gitbook/assets/Rufus 08.png>)

You now have a Bootable USB Drive with Ubuntu Server Installer on it. You can now use it to [Install Ubuntu Server](install-ubuntu-server.md) on the NAS PC.
